#include "bmp.h"
#include <stdbool.h>

// BM in ASCII. Constant that signals that the image is BMP or DIB. See BMP Format specification for details.
#define BMP_TYPE 0x4D42

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static inline uint64_t padding_for(const uint64_t width) {
	const uint64_t row_size = width * sizeof(struct pixel);
	const uint64_t padding = 4 - (row_size % 4);
	if(padding == 4) {
		return 0; // if to reach the dword we need a dword, we're basically stacked
	}

	return padding;
}

static inline uint64_t size_of_bitmap(const struct image* img) {
	const uint64_t row_sz = img->width * sizeof(struct pixel) + padding_for(img->width);
	const uint64_t data_sz = row_sz * img->height;
	return data_sz;
}

static inline uint64_t size_of_bmp(const struct image* img){
	const uint64_t header_sz = sizeof(struct bmp_header);
	const uint64_t data_sz = size_of_bitmap(img);
	return header_sz + data_sz;
}

static inline struct bmp_header bmp_header_for(const struct image* img) {
	return (struct bmp_header) {
		.bfType = BMP_TYPE,
		.bfileSize = size_of_bmp(img),
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = 40, //sizeof(Windows' BITMAPINFOHEADER)
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = 1, //Must be 1, artifact of the past
		.biBitCount = sizeof(struct pixel) * 8, //bits per pixel
		.biCompression = 0, //We do not compress this one
		.biSizeImage = size_of_bitmap(img),
		.biXPelsPerMeter = 0, //ignoring resolution
		.biYPelsPerMeter = 0,
		.biClrUsed = 0, // default number of colors
		.biClrImportant = 0 // every color is important so 0
	};
}

static inline bool write_bmp_header(FILE* out, const struct bmp_header* const h) {
	return fwrite(h, sizeof(struct bmp_header), 1, out);
}

static inline bool write_pixel_row(FILE* out, const struct pixel* const row, const uint64_t width) {
  return fwrite(row, sizeof(struct pixel), width, out) == width;
}

static inline bool write_row_padding(FILE* out, const uint64_t row_width) {
  static const uint64_t zeros = 0;
  return fwrite(&zeros, padding_for(row_width), 1, out);
}

static inline bool write_bmp_bitmap(FILE* out, struct image const* img) {	
  const uint64_t w = img->width;
  const uint64_t h = img->height;
  const struct pixel* row = img->data;

  for(uint64_t i = 0; i < h; i++) {
    if(!write_pixel_row(out, row, w)){
      return false;
    }

    if(!write_row_padding(out, w)) {
      return false; //couldn't append the padding
    }  
    row += w;
  }

  return true;
}

enum write_status img_to_bmp(FILE* out, struct image const* img) {
	const struct bmp_header h = bmp_header_for(img);
	if(!write_bmp_header(out, &h)) {
		return WRITE_BAD_HEADER;
	}
	
	fseek(out, h.bOffBits, SEEK_SET);

	if(!write_bmp_bitmap(out, img)) {
		return WRITE_BAD_DATA;
	}

	return WRITE_OK;
}


static inline bool read_header(FILE* in, struct bmp_header * const header) {
	return fread(header, sizeof(struct bmp_header), 1, in);
}

static inline bool read_pixel_row(
	FILE* in,
	struct pixel* row,
	uint64_t width
) {
	return fread(row, sizeof(struct pixel), width, in);
}

static inline bool read_data(
	FILE* in, 
	const struct bmp_header * const header,
	struct image * img
) {
	const uint64_t padding = padding_for(header->biWidth);
	struct pixel* row;

	*img = image_of_size(header->biWidth, header->biHeight);
	row = img->data;
	for(uint64_t i = 0; i < img->height; i++) {
    if(!read_pixel_row(in, row, img->width)) {
      return false;
    }
    
    if(fseek(in, padding, SEEK_CUR) != 0) {
      return false;
    }

    row += img->width;
  }

  return true;
}

enum read_status img_from_bmp(FILE* in, struct image * img) {
	struct bmp_header header;
	if(!read_header(in, &header)) {
		return READ_BAD_HEADER;
	}

	

	if(!read_data(in, &header, img)) {
		return READ_BAD_DATA;
	}

	return READ_OK;
}

