#include "img.h"
#include "transform.h"

struct image rotate_90_counter(struct image img) {
	const uint64_t w = img.width;
	const uint64_t h = img.height;
	const uint64_t new_w = h;
	//const uint64_t new_h = w;
	struct image t = image_of_size(h, w);

	for(uint64_t y = 0; y < h; y++) {
		for(uint64_t x = 0; x < w; x++) {
			const uint64_t new_x = h - 1 - y;
			const uint64_t new_y = x;
			t.data[new_x+new_y*new_w] = img.data[x+y*w];
		}
	}

	return t;
}
