#include "img.h"
#include <stdlib.h>
#include <string.h>

//image_of_size allocates a new blank image of size width X height
struct image image_of_size(const uint64_t width, const uint64_t height) {
	const uint64_t px_count = width * height;
	struct pixel* data = malloc(px_count * sizeof(struct pixel));
	return new_image(width, height, data);
}

void free_image(const struct image * img) {
	free(img->data);
}
