#include "bmp.h"
#include "img.h"
#include "transform.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc < 3) {
	puts("Bad argument count\n");
	return -1;
    }

    const char* const in_fname = argv[1];
    const char* const out_fname = argv[2];
    FILE* const in_file = fopen(in_fname, "r");
    FILE* out_file;

    struct image img;
    struct image res;

    const unsigned int r_res = img_from_bmp(in_file, &img);
    fclose(in_file);
    if(r_res > 0){
	    free_image(&img);
	    printf("error reading '%s': %d\n", in_fname, r_res);
	    return -1;
    }

    res = rotate_90_counter(img);
    free_image(&img);

    out_file = fopen(out_fname, "w");
    const unsigned int w_res = img_to_bmp(out_file, &res);
    fclose(out_file);
    if(w_res > 0) {
	    free_image(&res);
	    printf("error writing the result into '%s': %d\n", out_fname, w_res);
	    return -2;
    }
    free_image(&res);
    printf("transformed image can be found in '%s'\n", out_fname);
    return 0;
}
