#ifndef PIXEL_H
#define PIXEL_H
#include <stdint.h>

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image {
	uint64_t width, height;
	struct pixel* data;
};


static inline struct image new_image(
	const uint64_t width, 
	const uint64_t height,
	struct pixel * data
) {
	return (struct image) {
		.width = width,
		.height = height,
		.data = data
	};
}

struct image image_of_size(
	const uint64_t width,
	const uint64_t height
);

struct image image_copy(const struct image * const img);

void free_image(const struct image * img);
#endif
