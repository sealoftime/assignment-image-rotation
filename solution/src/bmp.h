#include "img.h"
#include  <stdint.h>
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_BAD_HEADER,
	READ_BAD_DATA
};

enum read_status img_from_bmp( FILE* in, struct image* img);

enum write_status {
	WRITE_OK = 0,
	WRITE_BAD_HEADER,
	WRITE_BAD_DATA
};

enum write_status img_to_bmp( FILE* out, struct image const* img);
